# RHEL6 requires a custom resolv.conf to deal with a single-threaded lookup
# bug which reduces performance to a crawl for services like sshd.
#
# TODO: remove EL6 custom resolv.conf when this bug is fixed.

# $priority_dns_servers: To add a set of DNS servers ahead of the anycast
# servers (but behind the localhost if caching is enabled) provide them as
# an array.
define su_dns::resolv_conf (
  Enum['present', 'absent'] $ensure               = undef,
  Boolean                   $enable_dns_cache     = false,
  Array[String]             $priority_dns_servers = [],
  Boolean                   $is_dns_server        = false ,
  Array[String]             $anycast_servers      = ['171.64.1.234', '171.67.1.234'],
) {

  if ($::lsbdistcodename == 'santiago') {
    $set_dns_options = true
    $dns_options     = 'single-request-reopen'
  } else {
    $set_dns_options = false
  }

  # resolv.conf is constructed from a template
  if ($is_dns_server) {
    $dns_server_name = $::hostname
  }

  file { '/etc/resolv.conf':
    ensure  => $ensure,
    content => template('su_dns/etc/resolv.conf.erb'),
  }
}
