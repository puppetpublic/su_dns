class su_dns::cache::bind (
  Enum['present', 'absent'] $ensure,
  Integer                   $max_cache_size_bytes,
  Boolean                   $manage_rfc1918_zone = true,
  Boolean                   $forwarding_only     = true,
  Array[String]             $anycast_servers     = [],
  Boolean                   $dnssec_enable       = true,
){

  # Install the bind packages.
  package {
    'bind9':     ensure => $ensure;
    'bind9-doc': ensure => $ensure;
  }

  # Configure named.conf.options
  file { '/etc/bind/named.conf.options':
    ensure  => $ensure,
    content => template('su_dns/etc/bind/named.conf.options.erb'),
    require => Package['bind9'],
    notify  => Service['bind9'],
  }

  # Configure named.conf.local
  file { '/etc/bind/named.conf.local':
    ensure  => $ensure,
    content => template('su_dns/etc/bind/named.conf.local.erb'),
    require => Package['bind9'],
    notify  => Service['bind9'],
  }

  if ($manage_rfc1918_zone) {
    # Deploy zones.rfc1918
    file { '/etc/bind/zones.rfc1918':
      ensure  => $ensure,
      content => template('su_dns/etc/bind/zones.rfc1918.erb'),
      require => Package['bind9'],
      notify  => Service['bind9'],
    }
  }


  if ($ensure == 'present') {
    $service_ensure = 'running'
  } else {
    $service_ensure = 'stopped'
  }

  service { 'bind9':
    ensure  => $service_ensure,
    require => File['/etc/bind/named.conf.options'],
  }
}
