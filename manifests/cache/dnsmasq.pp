class su_dns::cache::dnsmasq(
  Enum['present', 'absent'] $ensure,
  Integer                   $cache_size_entries = 10000,
){
  # JESSIE and beyond
  package { 'dnsmasq':
    ensure => $ensure,
  }

  # Call the template for /etc/dnsmasq.d/stanford-dnscache.conf. This
  # template uses the $cache_size_entries class parameter.
  file {'/etc/dnsmasq.d/stanford-dnscache.conf':
    ensure  => $ensure,
    content => template('su_dns/etc/dnsmasq.d/stanford-dnscache.conf.erb'),
    require => Package['dnsmasq'],
    notify  => Service['dnsmasq'],
  }

  # Define the service and make sure it runs. Restart the service if
  # /etc/resolv.conf changes.
  if ($ensure == 'present') {
    $service_ensure = 'running'
  } else {
    $service_ensure = 'stopped'
  }
  service { 'dnsmasq':
    ensure    => $service_ensure,
    require   => Package['dnsmasq'],
  }

}
