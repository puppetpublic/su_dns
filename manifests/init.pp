# Defines the DNS configuration of a system.

# You may set $manage_resolv_conf to false to prevent Puppet from
# overriding the system's resolv.conf file.  This is helpful in DHCP
# environments.

# If you want to enable a DNS cache, set $enable_dns_cache to true and set
# $cache_type to either "dnsmasq" or "bind". As a result 127.0.0.1 will be
# set as the first nameserver in /etc/resolv.conf. Any value other than
# "dnsmasq" or "bind" will result in an error.

# You can override the Stanford anycast servers if you need to.

class su_dns(
  Boolean                 $manage_resolv_conf   = true,
  Boolean                 $is_dns_server        = false ,
  Array[String]           $priority_dns_servers = [],
  Array[String]           $anycast_servers      = ['171.64.1.234', '171.67.1.234'],
  Boolean                 $dnssec_enable        = true,
  #
  # cache parameters
  Boolean                 $enable_dns_cache           = false,
  Enum['dnsmasq', 'bind'] $cache_type                 = 'bind',
  Integer                 $dnsmasq_cache_size_entries = 10000,
  Integer                 $bind_max_cache_size_bytes  = 50000000,
) {

  # DNS cache setup works only for Debian.
  if ($enable_dns_cache) {
    if ($::osfamily != 'Debian') {
      fail('su_dns caching only works with Debian')
    }
  }

  if ($enable_dns_cache) {
    # Install at most one of 'dnsmasq' or 'bind'.
    case $cache_type {
      'dnsmasq': {
        $ensure_dnsmasq = 'present'
        $ensure_bind    = 'absent'
      }
      'bind': {
        $ensure_dnsmasq = 'absent'
        $ensure_bind    = 'present'
      }
      default: {
        fail("unrecognized cache type")
      }
    }
  } else {
    # No DNS caching nameserver.
    $ensure_dnsmasq = 'absent'
    $ensure_bind    = 'absent'
  }

  # Set up /etc/resolv.conf.
  if ($manage_resolv_conf) {
    su_dns::resolv_conf { $::fqdn_lc:
      ensure               => 'present',
      enable_dns_cache     => $enable_dns_cache,
      priority_dns_servers => $priority_dns_servers,
      is_dns_server        => $is_dns_server,
      anycast_servers      => $anycast_servers,
    }
  }

  class { 'su_dns::cache::dnsmasq':
    ensure             => $ensure_dnsmasq,
    cache_size_entries => $dnsmasq_cache_size_entries,
  }

  class { 'su_dns::cache::bind':
    ensure               => $ensure_bind,
    max_cache_size_bytes => $bind_max_cache_size_bytes,
    anycast_servers      => $anycast_servers,
    dnssec_enable        => $dnssec_enable,
  }

}
