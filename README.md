[[_TOC_]]

# The su_dns Puppet module

## Overview

The `su_dns` Puppet module sets up `/etc/resolv.conf` for on-campus
servers and supports the running of local DNS caches.

## Basic use

If you want to set up your on-campus server to use Stanford's DNS service:

    include su_dns

Note that the DNS servers that will be included in `/etc/resolv.conf` are
Stanford's on-campus anycast DNS servers. In particular, these will *not*
work for cloud instances (unless an explicit network tunnel back to campus
has been set up).

## Caching DNS service (forwarding)

### Enabling BIND as the DNS caching service

Here is how you would setup [BIND](https://www.isc.org/downloads/bind/) as
your local DNS caching service:

    class { 'su_dns':
      enable_dns_cache => true,
      cache_type       => "bind",
      dnssec_enable    => false,
    }

Note that the BIND server is set up as a _forwarding_ DNS server, This
means the results are still cached, but **all** un-cached DNS requests are
forwarded to the Stanford anycast servers. Forwarding is necessary
otherwise DNS queries that resolve to private (RFC1918) addresses would
not resolve properly.  

Note: As of Debian Bookworm, the default dnssec-validation setting (auto) used by
bind behaves differently from previous versions of Debian.  This caused 
DNS lookups for .SUNET (or any local, non-public domain) to fail.  The fix is to
set the dnssec-validation to no which resolves this issue as is the [recommended
setting](https://web.stanford.edu/~riepel/dns/named.conf) to use for BIND for 
all DNS forwarding BIND servers.

### Enabling dnsmasq as the DNS caching service

Here is how you would setup
[`dnsmasq`](http://www.thekelleys.org.uk/dnsmasq/doc.html) as your local
DNS caching service:

    class { 'su_dns':
      enable_dns_cache => true,
      cache_type       => "dnsmasq",
    }

### Disabling DNS caching service

To remove the two above DNS caching services:

    class { 'su_dns':
      enable_dns_cache => false
    }


### Setting DNS caching size parameters

You can set the cache-size explicitly. To set the number of entries for
`dnsmasq` to 20000:

    class { 'su_dns':
      enable_dns_cache           => true,
      cache_type                 => 'dnsmasq',
      dnsmasq_cache_size_entries => 20000,
    }

For BIND, the cache size is in terms of bytes. To set BIND's maximum
cache size to 10 million bytes:

    class { 'su_dns':
      enable_dns_cache          => true
      cache_type                => 'bind'
      bind_max_cache_size_bytes => 10000000,
    }

### Dump the current BIND cache

To see what the BIND caching service currently has cached:

    $ /usr/sbin/rndc dumpdb -cache ; more /var/cache/bind/named_dump.db

### Flush current BIND cache

To see what the BIND caching service currently has cached:

    $ /usr/sbin/rndc flush


